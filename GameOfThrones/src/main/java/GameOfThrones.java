import java.util.Scanner;

/**
 * Created by HaChiXinhGai on 1/2/2015.
 */
public class GameOfThrones {
    static  int T;  // T < 1000
    protected static int size ;
    protected static int ar[] ;
    public static void main(String arg[]){
        Scanner input = new Scanner(System.in);
        T = input.nextInt();
        input.nextLine();
        int[] battle = new int[T];
        for(int i = 0; i < T; i++){
            battle[i] = input.nextInt();
        }
        battleDay();
        for(int j = 0; j < T; j++){
            battleOfThrones(battle[j]);
        }
    }

    private static void battleDay() {
        ar = new int[44720];
        size = 1;
        ar[1] = 1;
        for(int i = 2;i < 44720;i++){
            ar[++size] = ar[i-1] + i;
        }
    }

    private static void battleOfThrones(int battle) {
        int dayOfBattle;
        dayOfBattle = binary_search(battle);
        if(dayOfBattle%3 == 0){
            System.out.println("SL");
        }
        else if(dayOfBattle%3 == 1){
            System.out.println(("LB"));
        }
        else if (dayOfBattle%3 == 2){
            System.out.println("BS");
        }
    }

    private static int binary_search(int battle) {

        int low = 1, high = size ,mid;
        while(true){
            mid = (low + high)/2;
            if(ar[mid] == battle){
                return mid;
            }
            else if(ar[mid] <= battle && ar[mid + 1] >= battle){
                return mid + 1;
            }
            else if(ar[mid] < battle){
                low = mid + 1;
            }
            else {high = mid - 1;}
        }

    }

}
